#!/bin/bash
set -e
if [ "$ENV" = 'DEV' ]; then
  echo "Start Development Server"
  exec python "myapp.py"
else
  echo "Start Production Server"
  exec uwsgi --http 0.0.0.0:5000 --wsgi-file /app/myapp.py --callable app --stats 0.0.0.0:5001
fi
