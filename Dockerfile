FROM python:3.4
RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi
RUN pip install Flask==0.10.1 uWSGI
WORKDIR /app
COPY app /app
COPY cmd.sh /
EXPOSE 5000 5001
USER uwsgi
CMD ["/cmd.sh"]
